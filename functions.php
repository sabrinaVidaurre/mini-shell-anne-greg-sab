<?php
function calculPath($chemin, $command_args) {                                   
    //si absolu
    //deux windows ou linux
    //si windows
    
    $pattern = strpos(strtoupper($_SERVER['OS']), 'WINDOW') === 0?"#[a-z]:/?.*#i":"#/.*#"; // reconnaissance de l'os pour changer la racine
 
 
    if(preg_match($pattern ,$command_args)){
        if(!file_exists($command_args)){
            return false;
        }
        return $command_args;
    }
 
    $chemin = str_replace("\\","/",$chemin); // Remplace les \ par des /
    $command_args = str_replace("\\","/",$command_args);

 
    $partiesChemin = explode ("/",$command_args);
    
    foreach ($partiesChemin as $partie) {
        if(preg_match("#^\.{2}$#i",$partie)){ //si ..
            $chemin = dirname ($chemin);
        }else if(!preg_match("#^\.?$#i",$partie)){ // si .
            $chemin .= "/".$partie;
            $chemin = str_replace("//","/",$chemin);
        }      
    }
    if(!file_exists($chemin)){
        return false;
    }
    return $chemin;
}
function analyseCommande($lectureLine, &$commandLine, &$command_args, &$command_options, $est_decoupage_options = true)
{
    $lectureLine = trim($lectureLine);
    $commandLine = null;
    $command_args = [];
    $command_options = [];
    $matches = [];
    $patternArgs = "([^ -]+([-]?[^ -]*)*[-]?)|(?:(?:\"[^\"]+\")|(?:\'[^\']+\'))";

    $estValide = preg_match("#^([A-Za-z]\w*) *((?:(?: +(?:(?:-[A-Za-z0-9]+)|" . $patternArgs . "))+)*)$#", $lectureLine, $matches);

    if ($estValide) {
        $commandLine = $matches[1];
        $subject = preg_split("/[\s]*(\\\"[^\\\"]+\\\")[\s]*|" . "[\s]*('[^']+')[\s]*|" . "[\s,]+/", trim($matches[2]), 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        //récuperer les options
        $command_options = recupererOptions($subject, $est_decoupage_options);
        //récuperer les arguments
        $command_args = recupererArguments($subject);
    } else {
        echoWithColor("Saisie de la liste de commande incorrecte \n", COLOR_RED);
    }
}
function recupererArguments($subject)
{
    $patterns = ["#^\'([^\']+)\'$#", "#^\"([^\"]+)\"$#", "#^[^ -]+([-]?[^ -]*)*[-]?$#"];
    $replaces = ["$1", "$1", "$0"];
    $command_args = preg_filter($patterns, $replaces, $subject);
    $command_args = array_values($command_args);
    return $command_args;
}
function recupererOptions($subject, $est_decoupage_options)
{
    $patternOptions = '#^-([A-Za-z0-9]+)$#';
    $replaceOptions = '$1';
    $command_options_tmp = preg_filter($patternOptions, $replaceOptions, $subject);
    $command_options = [];
    if ($est_decoupage_options) {
        foreach ($command_options_tmp as $option) {;
            $command_options = array_merge($command_options, str_split($option));
        }
    } else {
        $command_options =  $command_options_tmp;
    }
    return $command_options;
}
function returnCommand (&$commandLine){
    
    if (! is_null($commandLine)){
        return $commandLine;
    }else
    echoWithColor ("Erreur, commande inexistante", COLOR_LIGHT_MAGENTA);
    echo PHP_EOL;
    

}

function afficherRep(string &$chemin){ // permet d'afficher les dossiers et sous dossiers jusqu'à arriver aux fichiers
    $allFiles = [];
    if (!preg_match("#[/\\\\]?\.\.?$#",$chemin)) {
        if (is_dir($chemin)){
            $dirContent = [];
            $dirContent = scandir($chemin);
            foreach ($dirContent as $elt) {
               $allFiles = array_merge($allFiles, afficherRep($chemin."/".$elt));
            }

        } else if (is_file($chemin)){
            $allFiles[] = $chemin;
        }
    }
    return $allFiles;
}


// function calculerChemin(string $cheminEnCours, string $chemindestination)
// {

//     //si windows 
//     if (getOS() === "Windows") {
//         $patternAbsolu = "#^[a-z]:.*$#i";
//     } else {
//         $patternAbsolu = "#^/.*$#i";
//     }
//     $cheminEnCours = str_replace("\", "/", $cheminEnCours);
//     $chemindestination = str_replace("\", "/", $chemindestination);
//     if (preg_match($patternAbsolu, $chemindestination)) {
//         if (!file_exists($chemindestination))
//             return false;
//         return $chemindestination;
//     }

//     $partieChemin = explode("/", $chemindestination);
//     foreach ($partieChemin as $unePartie) {
//         if (preg_match("#^\.{2}.*#", $unePartie)) {
//             $cheminEnCours = dirname($cheminEnCours, 1);
//         } else if (preg_match("#^[^.]+$#i", $unePartie)) {
//             $cheminEnCours = $cheminEnCours . "/" . $unePartie;  
//         }
//         $cheminEnCours = str_replace("\", "/", $cheminEnCours);
//         $cheminEnCours = str_replace("//", "/", $cheminEnCours);
//     }

//     if (!file_exists($cheminEnCours))
//         return false;

//     return $cheminEnCours;
// }


// function MyPs (){
        
//     $allProc = [];
//     $repertoire = scandir("/proc");
//     $repertoire = preg_filter("#^\d+$#","$0",$repertoire);
 
//     foreach ($repertoire as $pid) {
//         $processusEncours = [
//             "SER"=>null,
//             "PID"=> intval($pid), 
//             "%CPU"=>null,
//             "MEM"=>null,
//             "VSZ"=>null,
//             "RSS"=>null,
//             "TTY"=>"?",
//             "STAT"=>null,
//             "START"=>null,
//             "TIME"=>null];
        
//          $tousLeFichier=file("/proc/$pid/cmdline");
//         $processusEncours["COMMAND"] = $tousLeFichier[0];

//         $allProc[]=$processusEncours;
//         var_dump($allProc[2]);
// }




?>