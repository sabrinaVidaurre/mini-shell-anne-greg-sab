<?php
function mycd(string &$chemin, $command_args){
  
    if (count($command_args) != 1){
        echoWithColor("  ----> Nombre d'argument incorrect", COLOR_LIGHT_MAGENTA) ;
        echo PHP_EOL;
        
    }
    else {
        $nouveauChemin =calculPath($chemin,$command_args[0]); 
        if($nouveauChemin===false)
           echoWithColor("Erreur, chemin inexistant \n", COLOR_MAGENTA) ;
        else
           $chemin = $nouveauChemin; 
    }
   
    
}
?>