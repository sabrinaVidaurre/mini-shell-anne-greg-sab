<?php
function MyMkdir (&$chemin,&$command_args, &$command_options){
 
    if (is_dir($chemin)) {

        if ($command_args) {

            echo PHP_EOL;
            echo ("  Répertoire : ".$chemin." \n");
            echo PHP_EOL;
            echo ("    Dernière MAJ                   Nom \n");
            echo ("    ----------------              -------- \n");

            foreach ($command_args as $arg){
                $cheminNewArg = $chemin."/".$arg;

                if (!file_exists($cheminNewArg)){

                    foreach ($command_options as $option) { 
                       
                        if ($option === "p") { // si option p
                            $cheminNewArg = $chemin;
                            $decoupe = preg_split ("#/#",$arg);
            
                            foreach ($decoupe as $sousdossier) {
                                $creation= mkdir($sousdossier);
                                
                               $cheminNewArg= $cheminNewArg."/".$sousdossier;// a revoir pb

                            }
                            
                        }
                        else{
                            echoWithColor ("Option inconnue", COLOR_MAGENTA);
                            break; // si autre chose que p
                        }
                  
                    }

                    if (empty($command_options)) { // si p oublié pour dossier parent
                        
                            if (!preg_match('#^[^\-][A-Za-z]+([\.\-_]*[A-Za-z0-9]+)*$#', $arg)) {
                                echoWithColor ("Pas d'option indiqué pour traiter cette demande", COLOR_MAGENTA);
                                break;
                             }else
                             $creation= mkdir($cheminNewArg);
                      
                       
                    }
          
                   
                    $maj = date ("d-m-Y H:i:s", filectime ($cheminNewArg));
                    echo ("   ".$maj."            ".$arg."\n");
                
                }
                else 
                echoWithColor("   -----> Ce répertoire existe déja", COLOR_MAGENTA);
                echo PHP_EOL;
            }
        }
        else {
            echoWithColor("   -----> Pas d'arguments demandés", COLOR_MAGENTA);
            echo PHP_EOL;
       
        }
    }


}


?>