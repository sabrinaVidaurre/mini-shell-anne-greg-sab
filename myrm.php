<?php

function MyRm (&$chemin, &$command_args){
    if (is_dir($chemin)) {

        if ($command_args) {
                      
            foreach ($command_args as $arg){
               
                $cheminNewArg = realpath ($chemin."/".$arg);
              
              if (file_exists($cheminNewArg)) {
                    $supp= rmdir($cheminNewArg);
                    echoWithColor ("Suppression effectuée du répertoire \"".$arg."\"", COLOR_GREEN);
                    echo PHP_EOL;
              } 
              else
              echoWithColor("  -----> Chemin invalide ou répertoire déja effacé ...", COLOR_MAGENTA);
              echo PHP_EOL;
                               
            }
        }
        else {
            echoWithColor("   -----> Pas d'arguments saisis !", COLOR_MAGENTA);
            echo PHP_EOL;
    
        }
    
    }
    else {
        echoWithColor("Problème avec le chemin... introuvable", COLOR_MAGENTA);
    }


}

?>