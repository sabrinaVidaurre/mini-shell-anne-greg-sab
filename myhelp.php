<?php
function help (){
    echo PHP_EOL;
    echo ("                  ------------ MyHELP ---------------");
    echo PHP_EOL;
    echo PHP_EOL;

    echoWithColor ("   - MyCd : \n", COLOR_LIGHT_CRAY);
    echo ("          Permet de se déplacer d'un répertoire à un autre. \n");
    echo "\n";
    echoWithColor ("   - Myls : \n", COLOR_LIGHT_CRAY);
    echo ("          Afficher le contenu du dossier en cours. \n");
    echo ("          ----- Options : -l --> Utiliser un format de liste longue \n");
    echo ("          ----- Options : -i --> Afficher le numéro d'index de chaque fichier \n");
    echo ("\n");
    echoWithColor ("   - MyPwd : \n", COLOR_LIGHT_CRAY);
    echo ("          Affiche le chemin du dossier en cours. \n");
    echo ("\n");
    echoWithColor ("   - MyMkdir : \n", COLOR_LIGHT_CRAY);
    echo ("          Permet de créer un nouveau répertoire, instantanément créé sur le répertoire en cours.\n");
    echo ("          ----- Options : -p --> création de toute l'arborescence menant au dossier si elle n'existait pas. \n");
    echo ("\n");
    echoWithColor ("   - MyMv : \n", COLOR_LIGHT_CRAY);
    echo ("          Permet de déplacer un fichier ou un dossier dans un répertoire indiqué.\n");
    echo ("          ----- Options : -f --> Ecrase les fichiers de destinations sans confirmation \n");
    echo ("          ----- Options : -i --> Demande confirmation avant d'écraser \n");
    echo ("          ----- Options : -u --> N'écrase pas le fichier de destination si celui-ci est plus récent. \n");
    echo ("\n");
    echoWithColor ("   - MyCp : \n", COLOR_LIGHT_CRAY);
    echo ("          Permet de créer une copie d'un fichier ou un dossier dans un répertoire indiqué.\n");
    echo ("          ----- Options : -R --> Copier un répertoire et tout son contenu y compris les éventuels sous répertoires  \n");
    echo ("          ----- Options : -r --> Copier un répertoire et tout son contenu y compris les éventuels sous répertoires  \n");
    echo ("\n");
    echoWithColor ("   - MyFInd : \n", COLOR_LIGHT_CRAY);
    echo ("          Permet de rechercher une chaîne de caractères dans un ou plusieurs fichiers.\n");
    echo ("          ----- Options : -name --> Recherche d'un fichier par son nom \n");
    echo ("          ----- Options : -type --> Recherche de fichier d'un certains type. \n");
    echo ("          ----- Options : -size -->  \n");
    echo ("          ----- Options : -atime --> Recherche par date de dernier accès \n");
    echo ("          ----- Options : -mtime --> Recherche par date de dernière modification \n");
    echo ("          ----- Options : -ctime -->   \n");
    echo ("          ----- Options : -user --> Recherche de fichiers appartenant à l'utilisateur donné \n");
    echo ("\n");
    echoWithColor ("   - MyRmdir : \n", COLOR_LIGHT_CRAY);
    echo ("          Permet de supprimer un répertoire vide.\n");
    echo ("          ----- Options : -p --> Supprime un dossier. \n");
    echo ("          ----- Options : -r --> Supprimer un répertoire et les sous-réperoits.  \n");    
    echo ("          ----- Options : -d -->  \n");
    echo ("          ----- Options : -i --> Demande si chaque fichier ou répertoire doit être supprimer \n");
    echo ("          ----- Options : -R -->  \n");
    echo ("\n");
    echoWithColor ("   - MyTail : \n", COLOR_LIGHT_CRAY);
    echo ("          Permet d'extraire les dernières lignes d'un document texte.\n");
    echo ("          ----- Options : -n -->  \n");
       echo ("\n");
    echoWithColor ("   - MyPs : \n", COLOR_LIGHT_CRAY);
    echo ("         Liste des processus en cours d'exécution.\n");
    echo ("          ----- Options : -a -->  \n");
    echo ("          ----- Options : -u -->  \n");
    echo ("          ----- Options : -x -->  \n");
    echo ("\n");
    echoWithColor ("   - MyHelp : \n", COLOR_LIGHT_CRAY);
    echo ("          Documentation qui affiche les commandes du programme MyMiniShell.\n");
    echo ("\n");
    
}
?>